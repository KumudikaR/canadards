module.exports = function() {
    this.dbConnections = [];

    this.dbConnections["c"] = {
        host: process.env.EndPoint_rdsC,
        port: process.env.Port_rdsC,
        user: process.env.User_rdsC,
        password: process.env.Password_rdsC,
        database: "c"
    };
};